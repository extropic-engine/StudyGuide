-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: infinityjournal.db
-- Generation Time: Apr 24, 2013 at 06:54 AM
-- Server version: 5.3.12-MariaDB
-- PHP Version: 5.3.10-nfsn2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `studyguide`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `correct` tinyint(1) NOT NULL,
  PRIMARY KEY (`answer_id`),
  KEY `question_id` (`question_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`answer_id`, `question_id`, `value`, `correct`) VALUES
(1, 1, '\\(e^{-1}\\)', 1),
(2, 1, '\\(e^{-{1 \\over 2}}\\)', 0),
(3, 1, '\\(e\\)', 0),
(4, 1, '\\(e^{1 \\over 2}\\)', 0),
(5, 1, '\\(e^2\\)', 0),
(6, 1, '\\(e^{-2}\\)', 0);

-- --------------------------------------------------------

--
-- Table structure for table `concepts`
--

CREATE TABLE IF NOT EXISTS `concepts` (
  `concept_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`concept_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `concepts`
--

INSERT INTO `concepts` (`concept_id`, `name`, `description`) VALUES
(1, 'exponential random variable', 'A continuous random variable with parameter \\(\\lambda\\) whose probability density function is given by\r\n\r\n$$\r\nf(x) = \\left\\{\r\n\\begin{array}{lr}\r\n\\lambda e^{-\\lambda x} & \\text{if } x \\ge 0 \\\\\r\n0 & \\text{if } x \\lt 0\r\n\\end{array}\r\n\\right.\r\n$$\r\n\r\nfor some \\(\\lambda \\gt 0\\).');

-- --------------------------------------------------------

--
-- Table structure for table `link_questions_concepts`
--

CREATE TABLE IF NOT EXISTS `link_questions_concepts` (
  `question_id` int(11) NOT NULL,
  `concept_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `link_questions_concepts`
--

INSERT INTO `link_questions_concepts` (`question_id`, `concept_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `link_topics_concepts`
--

CREATE TABLE IF NOT EXISTS `link_topics_concepts` (
  `topic_id` int(11) NOT NULL COMMENT 'Should be the "lowest" topic that the concept applies to.',
  `concept_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `link_topics_concepts`
--

INSERT INTO `link_topics_concepts` (`topic_id`, `concept_id`) VALUES
(14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_text` text NOT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`question_id`, `question_text`) VALUES
(1, 'The time (in hours) required to repair a machine is an exponentially distributed random variable with parameter \\(\\lambda = {1 \\over 2}\\). What is the probability that the time to repair a machine exceeds 2 hours?'),
(2, 'what color are you'),
(3, 'nope'),
(4, 'You arrive at a bus stop at 10 o''clock, knowing that the bus will arrive at some time uniformly distributed between 10 and 10:30. What is the probability that you will have to wait longer than 10 minutes?'),
(5, 'If \\(X\\) is an exponential random variable with parameter \\(\\lambda = 1\\), compute the probability density function of the random variable \\(Y\\) defined by \\(Y = log X\\).'),
(6, 'Jones figures that the total number of thousands of miles that an auto can be driven before it would need to be junked is an exponential random variable with parameter \\(1 \\over 20\\). Smith has a used car that he claims has been driven only 10,000 miles. If Jones purchases the car, what is the probability that she would get at least 20,000 additional miles out of it?');

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`topic_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`topic_id`, `parent_id`, `name`) VALUES
(1, 0, 'Chemistry'),
(2, 0, 'Mathematics'),
(3, 0, 'Languages'),
(4, 0, 'Computer Science'),
(5, 0, 'History'),
(6, 0, 'Literature'),
(7, 0, 'Geology'),
(8, 0, 'Philosophy'),
(9, 0, 'Psychology'),
(10, 0, 'Political Science'),
(11, 0, 'Architecture'),
(12, 2, 'Probability'),
(13, 1, 'Reaction Kinetics'),
(14, 12, 'Continuous Random Variables'),
(15, 2, 'Calculus'),
(16, 0, 'Sociology'),
(17, 0, 'Biology'),
(18, 0, 'Astronomy'),
(19, 0, 'Law'),
(20, 0, 'Electrical Engineering'),
(21, 1, 'Electrochemistry'),
(22, 1, 'Thermodynamics'),
(23, 1, 'Stoichiometry'),
(24, 1, 'Gases'),
(25, 4, 'Artificial Intelligence'),
(26, 4, 'Automata Theory'),
(27, 26, 'Finite State Machines'),
(28, 26, 'Turing Machines'),
(29, 26, 'Context-free Languages');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
