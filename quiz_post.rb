#!/usr/bin/env ruby
require 'rubygems'
require 'cgi'
require 'mysql'
require 'settings.rb'

begin
db = Mysql.real_connect(Settings.db_location, Settings.db_user, Settings.db_password, Settings.db_name)
cgi = CGI.new('html4')

question_id = db.escape_string(cgi['question_id'])
question_text = db.escape_string(cgi['question_text'])
module_id = db.escape_string(cgi['module_id'])

answers = []
i = 0
until cgi["answers[#{i}][answer_id]"] == "" do
  answers << { 'id' => db.escape_string(cgi["answers[#{i}][answer_id]"]),
             'text' => db.escape_string(cgi["answers[#{i}][answer_text]"])}
  i += 1
end

if question_id != "" and question_text != ""
  db.query("UPDATE questions SET question_text='#{question_text}' WHERE question_id='#{question_id}'")
elsif question_text != "" and module_id != ""
  db.query("INSERT INTO questions (question_text) VALUES ('#{question_text}')")
  result = db.query("SELECT LAST_INSERT_ID() as question_id")
  question_id = result.fetch_hash['question_id']
  db.query("INSERT INTO module_questions (module_id, question_id) VALUES (#{module_id}, #{question_id})")
end

answers.each do |answer|
  if answer['id'] != "" and answer['text'] != ""
    db.query("UPDATE answers SET value='#{answer['text']}' WHERE answer_id='#{answer['id']}'")
  end
end

cgi.out {"thanks for all the fish"}

ensure
db.close if db
end
