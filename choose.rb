#!/usr/bin/env ruby
require 'rubygems'
require 'liquid'
require 'cgi'
require 'mysql'
require 'settings.rb'

begin
db = Mysql.real_connect(Settings.db_location, Settings.db_user, Settings.db_password, Settings.db_name)
cgi = CGI.new('html4')

error = ''
subtopics = []
topic = {}

=begin
# get modules for this topic
result = db.query("SELECT module_id, name FROM modules WHERE topic_id=#{cgi['topic']}")

modules = Array.new
result.each_hash do |row|
  modules << {'name' => row['name'], 'id' => row['module_id']}
end

result.free
=end

# get subtopics for this topic
begin
  result = db.query("SELECT topic_id, name FROM topics WHERE parent_id=#{cgi['topic']} OR topic_id=#{cgi['topic']}")
  
  result.each_hash do |row|
    if row['topic_id'] == cgi['topic']
      topic = {'name' => row['name'], 'id' => row['topic_id']}
    else
      subtopics << {'name' => row['name'], 'id' => row['topic_id']}
    end
  end

  result.free
rescue Exception => e
  error = e.message + '<br>' + e.backtrace.inspect + '<br>db query failed'
end

# parse and render the template
template = Liquid::Template.parse(File.open("_choose.html", 'rb').read)
cgi.out {
  begin
    template.render('error' => error, 'topic' => topic, 'subtopics' => subtopics)
  rescue Exception => e
    template.render('error' => e.message + '<br>' + e.backtrace.inspect + '<br>template rendering failed')
  end
}

ensure
db.close if db
end
