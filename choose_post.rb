#!/usr/bin/env ruby
require 'rubygems'
require 'cgi'
require 'mysql'
require 'settings.rb'

begin
db = Mysql.real_connect(Settings.db_location, Settings.db_user, Settings.db_password, Settings.db_name)
cgi = CGI.new('html4')

parent_id = db.escape_string(cgi['parent_id'])
new_topic_name = db.escape_string(cgi['new_topic_name'])
new_module_name = db.escape_string(cgi['new_module_name'])

if new_topic_name != "" and parent_id != ""
  db.query("INSERT INTO topics (parent_id, name) VALUES('#{parent_id}', '#{new_topic_name}')")
elsif new_module_name != "" and parent_id != ""
  db.query("INSERT INTO modules (topic_id, name) VALUES('#{parent_id}', '#{new_module_name}')")
end

cgi.out {"thanks for all the fish"}

ensure
db.close if db
end
