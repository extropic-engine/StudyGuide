#!/usr/bin/env ruby
require 'rubygems'
require 'liquid'
require 'cgi'
require 'mysql'
require 'settings.rb'

begin
db = Mysql.real_connect(Settings.db_location, Settings.db_user, Settings.db_password, Settings.db_name)

result = db.query("SELECT topic_id, name FROM topics WHERE parent_id=0 ORDER BY name")

topics = Array.new
result.each_hash do |row|
  topics << {'id' => row['topic_id'], 'name' => row['name']}
end

result.free

# parse and render the template
template = Liquid::Template.parse(File.open("_index.html", 'rb').read)
CGI.new('html4').out {
  template.render('topics' => topics)
}

ensure
db.close if db
end
