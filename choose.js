$(document).ready(function() {
/*
// click an answer
$('li').click(function() {
  // do some ajax to check result... for now, pretend like you're wrong
  var result = false;

  if (result == true) {
    $(this).css('color', 'green');
  } else {
    $(this).css('color', 'red'); //addClass('.correct');
  }
});
*/
/**********
 * editor *
 **********/
/*
// edit elements hidden by default
$(".edit textarea").hide();
$('.question_edit').hide();
$('.answer_edit').hide();
$('.correct').hide();
$('.incorrect').hide();
$('.add_answer').hide();

// click to add a new question
$(".edit").click(function() {
  $(".edit_text").html('Add Question');
  $('.edit textarea').val('Enter your question here.');
  $(".edit textarea").show();
});

*/

// add a topic
$("input#add_topic").keypress(function(e) {
  if (e.which == 13) {
    e.preventDefault();
    jQuery.post('choose_post.rb', { new_topic_name: $(this).val(), parent_id: $('input[type=hidden]#topic_id').val() });
    $(this).val('');
    return false;
  }
});

// add a module
$("input#add_module").keypress(function(e) {
  if (e.which == 13) {
    e.preventDefault();
    jQuery.post('choose_post.rb', { new_module_name: $(this).val(), parent_id: $('input[type=hidden]#topic_id').val() });
    $(this).val('');
    return false;
  }
});

/*

// click to edit a question
$(".question_box").on("click", ".edit_question.saved", function() {
  // change the "edit" button to "save button
  $(this).text('Save');
  $(this).addClass('unsaved').removeClass('saved');

  // hide text and replace it with input boxes
  $(this).siblings(".question_text").hide();
  $(this).siblings(".question_edit").show();
  $(this).siblings(".answers").find('ol li').each(function(i) {
    $(this).children('.answer_edit').show();
    $(this).children('.answer_text').hide();
    $(this).children('.incorrect').show();
  });
  $(this).siblings(".answers").find(".add_answer").show();
});

// click to save an edited question
$('.question_box').on("click", ".edit_question.unsaved", function() {
  // change the "save" button back to an "edit" button
  $(this).text('Edit');
  $(this).addClass('saved').removeClass('unsaved');

  // apply edits to the question text and hide/show the appropriate things
  var question = $(this).siblings(".question_text");
  var question_edit = $(this).siblings(".question_edit");
  question.html($.trim(question_edit.val()));
  question_edit.hide();
  question.show();
  $(this).siblings(".answers").find(".add_answer").hide();

  // apply edits to the answer text and hide/show the appropriate things
  var answers = [];
  $(this).siblings(".answers").find("ol li").each(function(i) {
    var answer_edit = $(this).children('.answer_edit').find('input');
    var answer_text = $(this).children('.answer_text');
    answer_text.html(answer_edit.val());
    $(this).children('.answer_edit').hide();
    $(this).children('.incorrect').hide();
    $(this).children('.correct').hide();
    answer_text.show();
    answers.push({ answer_id: $(this).children('#answer_id').val(), answer_text: answer_edit.val() });
  });

  // do some ajax magic to save this stuff to the database
  jQuery.post('quiz_post.rb', { question_id: $(this).siblings('#question_id').val(), question_text: question_edit.val(), answers: answers});

  // recalculate math formulas in case anything changed
  MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
});

// add answer
$('.question_box').on('click', '.add_answer', function() {
  var list = $(this).siblings('ol');
  var new_answer = jQuery(document.createElement('li'));
  new_answer.append("<span class='answer_text'></span>");
  new_answer.append("<span class='answer_edit'><input type='text' value=''></span>");
  new_answer.append("<input type='hidden' id='answer_id' value='999'>");
  new_answer.append("<span class='correct'>Correct</span>");
  new_answer.append("<span class='incorrect'>Incorrect</span></li>");
  new_answer.children('.answer_text').hide();
  new_answer.children('.correct').hide();
  list.append(new_answer);
});

// toggle correct/incorrect
$('.question_box').on('click', 'span.correct', function() {
  $(this).siblings('.incorrect').show();
  $(this).hide();
});
$('.question_box').on('click', 'span.incorrect', function() {
  $(this).siblings('.correct').show();
  $(this).hide();
});
*/
});
