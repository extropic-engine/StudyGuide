#!/usr/bin/env ruby
require 'rubygems'
require 'liquid'
require 'cgi'
require 'mysql'
require 'settings.rb'

begin
db = Mysql.real_connect(Settings.db_location, Settings.db_user, Settings.db_password, Settings.db_name)
cgi = CGI.new('html4')

answered = 0
right = false
if cgi['answer'] != ""
  answered = cgi['answer']
  result = db.query("SELECT correct FROM answers WHERE answer_id=#{cgi['answer']}")
  if (result.fetch_hash['correct'] == '1')
    right = true
  end
end

# get modules for this topic
#result = db.query("SELECT question_id, question_text FROM questions JOIN module_questions USING (question_id) WHERE module_id=#{cgi['id']}")

# TODO: also select all questions that are linked to subtopics of this topic
result = db.query("SELECT question_id, question_text FROM questions
                              JOIN link_questions_concepts USING (question_id)
                              JOIN link topics_concepts using (concept_id) WHERE topic_id=#{cgi['id']}")

questions = Array.new
result.each_hash do |row|
  answer_result = db.query("SELECT answer_id, value FROM answers WHERE question_id=#{row['question_id']}")
  answers = Array.new
  answer_result.each_hash do |ans_row|
    status = 'unanswered'
    if ans_row['answer_id'] == answered
      if right
        status = 'right'
      else
        status = 'wrong'
      end
    end
    answers << {'id' => ans_row['answer_id'], 'value' => ans_row['value'], 'status' => status}
  end
  answer_result.free
  questions << {'id' => row['question_id'], 'text' => row['question_text'], 'answers' => answers}
end

result.free

# parse and render the template
template = Liquid::Template.parse(File.open("_quiz.html", 'rb').read)
cgi.out {
  begin
    template.render('id' => cgi['id'], 'questions' => questions)
  rescue Exception => e
    template.render('error' => e.message + '<br>' + e.backtrace.inspect + '<br>template rendering failed')
  end
}

ensure
db.close if db
end
